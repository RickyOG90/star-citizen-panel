import tkinter as tk
from tkinter import *
from tkinter import font as tkFont
from tkinter.simpledialog import askstring
from PIL import ImageTk, Image
from flask import (Flask, render_template)
import requests as re
import cv2
import os

root = Tk()
sizeFont = tkFont.Font(size = 12, weight = "bold")
sizeFont2 = tkFont.Font(size = 14, weight ="bold")

#----------------------------------------------------------------------------------------------------
#Placeholder Server Address	

url = '0.0.0.0'

#----------------------------------------------------------------------------------------------------
#Flight Controls

def toggle_mobiglass():
	url2 = url + 'keypress/f1'
	re.get(url2)

def toggle_starmap():
	url2 = url + 'keypress/f2'
	re.get(url2)
	
def toggle_camera_view():
	url2 = url + 'keypress/f4'
	re.get(url2)
	
def incr_eng_pwr():
	url2 = url + 'keypress/f5'
	re.get(url2)

def incr_shld_pwr():
	url2 = url + 'keypress/f6'
	re.get(url2)
	
def incr_weap_pwr():
	url2 = url + 'keypress/f7'
	re.get(url2)
	
def reset_pwr():
	url2 = url + 'keypress/f8'
	re.get(url2)

def decr_pwr():
	url2 = url + 'keypress/f9'
	re.get(url2)
	
def incr_pwr():
	url2 = url + 'keypress/f10'
	re.get(url2)

def toggle_contacts():
	url2 = url + 'keypress/f11'
	re.get(url2)

def first_target():
	url2 = url + 'keypress/1'
	re.get(url2)

def pin_first_target():
	url2 = url + 'hotkey/alt1pt2'
	re.get(url2)
	url2 = url + 'hotkey/alt1pt2'
	re.get(url2)

def second_target():
	url2 = url + '2'
	re.get(url2)

def pin_second_target():
	url2 = url + 'hotkey/alt2'
	re.get(url2)

def third_target():
	url2 = url + '3'
	re.get(url2)

def pin_third_target():
	url2 = url + 'hotkey/alt3'
	re.get(url2)

def cycle_attackers():
	url2 = url + '4'
	re.get(url2)

def reset_attackers():
	url2 = url + 'hotkey/alt4'
	re.get(url2)

def cycle_hostiles():
	url2 = url + '5'
	re.get(url2)

def reset_hostiles():
	url2 = url + 'hotkey/alt5'
	re.get(url2)

def cycle_friendlies():
	url2 = url + '6'
	re.get(url2)

def reset_friendlies():
	url2 = url + 'hotkey/alt6'
	re.get(url2)

def cycle_all():
	url2 = url + '7'
	re.get(url2)

def reset_all():
	url2 = url + 'hotkey/alt7'
	re.get(url2)

def cycle_subs():
	url2 = url + 'keypress/8'
	re.get(url2)
	
def reset_subs():
    url2 = url + 'hotkey/alt8'
    re.get(url2)

def hail_target():
	url2 = url + 'keypress/9'
	re.get(url2)

def clear_pin_targets():
	url2 = url + 'keypress/0'
	re.get(url2)

def respawn_destruct():
    url2 = url + 'hotkey/backspace'
    re.get(url2)

def toggle_scanner():
	url2 = url + 'keypress/tab'
	re.get(url2)

def flight_ready():
	url2 = url + 'keypress/p'
	re.get(url2)

def reset_selection():
    url2 = url + 'hotkey/altr'
    re.get(url2)

def ret_lock():
	url2 = url + 'keypress/t'
	re.get(url2)

def ret_unlock():
    url2 = url + 'hotkey/altt'
    re.get(url2)
	
def get_out():
	url2 = url + 'keypress/y'
	re.get(url2)
	re.get(url2)

def seat_eject():
	url2 = url + 'keypress/l'
	re.get(url2)
	re.get(url2)

def toggle_pwr():
	url2 = url + 'keypress/u'
	re.get(url2)

def em_seat_exit():
    url2 = url + 'hotkey/shiftu'
    re.get(url2)
	
def toggle_engs():
	url2 = url + 'keypress/i'
	re.get(url2)
	
def toggle_shlds():
	url2 = url + 'keypress/o'
	re.get(url2)

def toggle_weaps():
	url2 = url + 'keypress/p'
	re.get(url2)

def accept_invite():
	url2 = url + 'keypress/['
	re.get(url2)

def reject_invite():
	url2 = url + 'keypress/]'
	re.get(url2)

def toggle_gimbal():
	url2 = url + 'keypress/g'
	re.get(url2)
	
def launch_cm():
	url2 = url + 'keypress/h'
	re.get(url2)

def cycle_cm():
	url2 = url + 'keypress/j'
	re.get(url2)

def jettison_Cargo():
    url2 = url + 'hotkey/j'
    re.get(url2)

def toggle_vtol():
	url2 = url + 'keypress/k'
	re.get(url2)

def toggle_lights():
	url2 = url + 'keypress/l'
	re.get(url2)

def toggle_look_forward():
    url2 = url + 'hotkey/l'
    re.get(url2)

def toggle_cruise_control():
	url2 = url + 'keypress/c'
	re.get(url2)

def toggle_decoupled():
	url2 = url + 'keypress/v'
	re.get(url2)

def spool_qt():
	url2 = url + 'keypress/b'
	re.get(url2)

def engage_qt():
    url2 = url + 'hotkey/b'
    re.get(url2)

def re_lg():
	url2 = url + 'keypress/n'
	re.get(url2)

def auto_land():
    url2 = url + 'hotkey/n'
    re.get(url2)

def toggle_mining():
	url2 = url + 'keypress/m'
	re.get(url2)

def toggle_ai_turrets():
	url2 = url + 'keypress//'
	re.get(url2)

def shld_raise_top():
	url2 = url + 'keypress/numpad_7'
	re.get(url2)

def shld_raise_front():
	url2 = url + 'keypress/numpad_8'
	re.get(url2)

def shld_raise_bottom():
	url2 = url + 'keypress/numpad_9'
	re.get(url2)

def shld_raise_left():
	url2 = url + 'keypress/numpad_4'
	re.get(url2)

def shld_reset():
	url2 = url + 'keypress/numpad_5'
	re.get(url2)

def shld_raise_right():
	url2 = url + 'keypress/numpad_6'
	re.get(url2)

def shld_raise_back():
	url2 = url + 'keypress/numpad_2'
	re.get(url2)

def toggle_door_lock():
	url2 = url + 'keypress/.'
	re.get(url2)

def open_close_doors():
	url2 = url + 'keypress/,'
	re.get(url2)

#----------------------------------------------------------------------------------------------------
#Advanced Camera Controls

def incr_dof():
    url2 = url + 'hotkey/f4home'
    re.get(url2)

def decr_dof():
    url2 = url + 'hotkey/f4home'
    re.get(url2)

def z_offset_pos():
    url2 = url + 'hotkey/f4pageup'
    re.get(url2)

def z_offset_neg():
    url2 = url + 'hotkey/f4pagedown'
    re.get(url2)

def y_offset_pos():
    url2 = url + 'hotkey/f4up'
    re.get(url2)

def y_offsest_neg():
    url2 = url + 'hotkey/f4down'
    re.get(url2)

def x_offset_pos():
    url2 = url + 'hotkey/f4right'
    re.get(url2)

def x_offset_neg():
    url2 = url + 'hotkey/f4left'
    re.get(url2)

def reset_view():
    url2 = url + 'hotkey/f4num_*'
    re.get(url2)

def decr_fov():
    url2 = url + 'hotkey/f4num-'
    re.get(url2)

#----------------------------------------------------------------------------------------------------
#Foot Controls

def equip_sidearm():
	url2 = url + 'keypress/1'
	re.get(url2)

def equip_first_primary():
	url2 = url + 'keypress/2'
	re.get(url2)

def equip_second_primary():
	url2 = url + 'keypress/3'
	re.get(url2)

def equip_gadget():
	url2 = url + 'keypress/4'
	re.get(url2)

def cycle_melee():
	url2 = url + 'keypress/5'
	re.get(url2)

def mission_item():
	url2 = url + 'keypress/6'
	re.get(url2)

def unarmed():
	url2 = url + 'keypress/0'
	re.get(url2)

def holster():
    url2 = url + 'hotkey/rr'
    re.get(url2)

def flashlight():
	url2 = url + 'keypress/t'
	re.get(url2)

def inventory():
	url2 = url + 'keypress/i'
	re.get(url2)

def weap_custom():
	url2 = url + 'keypress/j'
	re.get(url2)

def self_heal():
	url2 = url + 'keypress/c'
	re.get(url2)

def fire_modes():
	url2 = url + 'keypress/v'
	re.get(url2)

def oxy_refill():
	url2 = url + 'keypress/b'
	re.get(url2)

#----------------------------------------------------------------------------------------------------
#Player Emotes

def emote_agree():
	url2 = url + 'keypress/6'
	re.get(url2)

def emote_angry():
	url2 = url + 'keypress/7'
	re.get(url2)

def emote_at_ease():
	url2 = url + 'keypress/8'
	re.get(url2)
	
def emote_attention():
	url2 = url + 'keypress/9'
	re.get(url2)

def emote_blah():
	url2 = url + 'keypress/0'
	re.get(url2)

def emote_bored():
	url2 = url + 'keypress/-'
	re.get(url2)

def emote_bow():
	url2 = url + 'keypress/='
	re.get(url2)

def emote_burp():
	url2 = url + 'keypress/\''
	re.get(url2)
	
def emote_cheer():
    url2 = url + 'hotkey/shift6'
    re.get(url2)

def emote_chicken():
    url2 = url + 'hotkey/shift7'
    re.get(url2)

def emote_clap():
    url2 = url + 'hotkey/shift8'
    re.get(url2)

def emote_come():
    url2 = url + 'hotkey/shift9'
    re.get(url2)

def emote_cry():
    url2 = url + 'hotkey/shift0'
    re.get(url2)

def emote_dance():
    url2 = url + 'hotkey/shift-'
    re.get(url2)

def emote_disagree():
    url2 = url + 'hotkey/shift='
    re.get(url2)

def emote_failure():
    url2 = url + 'hotkey/shift\''
    re.get(url2)

def emote_flex():
    url2 = url + 'hotkey/alt6'
    re.get(url2)

def emote_flirt():
    url2 = url + 'hotkey/alt7'
    re.get(url2)

def emote_gasp():
    url2 = url + 'hotkey/alt8'
    re.get(url2)

def emote_gloat():
    url2 = url + 'hotkey/alt9'
    re.get(url2)

def emote_greet():
    url2 = url + 'hotkey/alt0'
    re.get(url2)

def emote_laugh():
    url2 = url + 'hotkey/alt-'
    re.get(url2)

def emote_point():
    url2 = url + 'hotkey/alt='
    re.get(url2)

def emote_rude():
    url2 = url + 'hotkey/alt\''
    re.get(url2)

def emote_salute():
    url2 = url + 'hotkey/altshift6'
    re.get(url2)

def emote_sit():
    url2 = url + 'hotkey/altshift7'
    re.get(url2)

def emote_sleep():
    url2 = url + 'hotkey/altshift8'
    re.get(url2)

def emote_smell():
    url2 = url + 'hotkey/altshift9'
    re.get(url2)

def emote_stand():
    url2 = url + 'hotkey/stand'
    re.get(url2)

def emote_taunt():
    url2 = url + 'hotkey/altshift0'
    re.get(url2)

def emote_threaten():
    url2 = url + 'hotkey/altshift-'
    re.get(url2)

def emote_wait():
    url2 = url + 'hotkey/altshift='
    re.get(url2)

def emote_wave():
    url2 = url + 'hotkey/altshift\''
    re.get(url2)

def emote_whistle():
    url2 = url + 'hotkey/altshift\\'
    re.get(url2)

#----------------------------------------------------------------------------------------------------
#Loading in images 

images = []

def resource_path(relative_path):
	try:
		base_path = sys._MEIPASS
	except Exception:
		base_path = os.path.abspath(".")
	return os.path.join(base_path, relative_path)


def load_images_from_folder(folder):
	for filename in os.listdir(folder):
		image = os.path.join(folder,filename)
		images.append(image)
	return images

load_images_from_folder(resource_path('Used Icons\\'))

#----------------------------------------------------------------------------------------------------
#Setting fullscreen by extracting user's screen resolution

w, h = root.winfo_screenwidth(), root.winfo_screenheight()

#----------------------------------------------------------------------------------------------------
#Calculations to scale everything based on scren resolution

menubar = round(h*0.1574)
imageWH = (h-menubar)/7
newWH = round(imageWH/150)

#----------------------------------------------------------------------------------------------------
#Images being adjusted to scale to user's resolution

photo1 = PhotoImage(file=images[0])
photo1 = photo1.zoom(newWH, newWH)
#photo1 = photo1.subsample(2)

photo2 = PhotoImage(file=images[4])
photo2 = photo2.zoom(newWH, newWH)

photo3 = PhotoImage(file=images[9])
photo3 = photo3.zoom(newWH, newWH)

photo4 = PhotoImage(file=images[10])
photo4 = photo4.zoom(newWH, newWH)

photo5 = PhotoImage(file=images[15])
photo5 = photo5.zoom(newWH, newWH)

photo6 = PhotoImage(file=images[16])
photo6 = photo6.zoom(newWH, newWH)

photo7 = PhotoImage(file=images[18])
photo7 = photo7.zoom(newWH, newWH)

photo8 = PhotoImage(file=images[19])
photo8 = photo8.zoom(newWH, newWH)

photo9 = PhotoImage(file=images[20])
photo9 = photo9.zoom(newWH, newWH)

photo10 = PhotoImage(file=images[21])
photo10 = photo10.zoom(newWH, newWH)

photo11 = PhotoImage(file=images[22])
photo11 = photo11.zoom(newWH, newWH)

photo12 = PhotoImage(file=images[23])
photo12 = photo12.zoom(newWH, newWH)

photo13 = PhotoImage(file=images[25])
photo13 = photo13.zoom(newWH, newWH)

photo14 = PhotoImage(file=images[26])
photo14 = photo14.zoom(newWH, newWH)

photo15 = PhotoImage(file=images[27])
photo15 = photo15.zoom(newWH, newWH)

photo16 = PhotoImage(file=images[28])
photo16 = photo16.zoom(newWH, newWH)

photo17 = PhotoImage(file=images[29])
photo17 = photo17.zoom(newWH, newWH)

photo18 = PhotoImage(file=images[31])
photo18 = photo18.zoom(newWH, newWH)

photo19 = PhotoImage(file=images[33])
photo19 = photo19.zoom(newWH, newWH)

photo20 = PhotoImage(file=images[34])
photo20 = photo20.zoom(newWH, newWH)

photo21 = PhotoImage(file=images[35])
photo21 = photo21.zoom(newWH, newWH)

photo22 = PhotoImage(file=images[36])
photo22 = photo22.zoom(newWH, newWH)

photo23 = PhotoImage(file=images[38])
photo23 = photo23.zoom(newWH, newWH)

photo24 = PhotoImage(file=images[39])
photo24 = photo24.zoom(newWH, newWH)

photo25 = PhotoImage(file=images[40])
photo25 = photo25.zoom(newWH, newWH)

photo26 = PhotoImage(file=images[42])
photo26 = photo26.zoom(newWH, newWH)

photo27 = PhotoImage(file=images[43])
photo27 = photo27.zoom(newWH, newWH)

photo28 = PhotoImage(file=images[46])
photo28 = photo28.zoom(newWH, newWH)

photo29 = PhotoImage(file=images[49])
photo29 = photo29.zoom(newWH, newWH)

photo30 = PhotoImage(file=images[50])
photo30 = photo30.zoom(newWH, newWH)

photo31 = PhotoImage(file=images[52])
photo31 = photo31.zoom(newWH, newWH)

photo32 = PhotoImage(file=images[53])
photo32 = photo32.zoom(newWH, newWH)

photo33 = PhotoImage(file=images[54])
photo33 = photo33.zoom(newWH, newWH)

photo34 = PhotoImage(file=images[55])
photo34 = photo34.zoom(newWH, newWH)

photo35 = PhotoImage(file=images[56])
photo35 = photo35.zoom(newWH, newWH)

photo36 = PhotoImage(file=images[57])
photo36 = photo36.zoom(newWH, newWH)

photo37 = PhotoImage(file=images[58])
photo37 = photo37.zoom(newWH, newWH)

photo38 = PhotoImage(file=images[59])
photo38 = photo38.zoom(newWH, newWH)

photo39 = PhotoImage(file=images[60])
photo39 = photo39.zoom(newWH, newWH)

photo40 = PhotoImage(file=images[62])
photo40 = photo40.zoom(newWH, newWH)

photo41 = PhotoImage(file=images[63])
photo41 = photo41.zoom(newWH, newWH)

photo42 = PhotoImage(file=images[64])
photo42 = photo42.zoom(newWH, newWH)

photo43 = PhotoImage(file=images[65])
photo43 = photo43.zoom(newWH, newWH)

photo44 = PhotoImage(file=images[66])
photo44 = photo44.zoom(newWH, newWH)

photo45 = PhotoImage(file=images[67])
photo45 = photo45.zoom(newWH, newWH)

photo46 = PhotoImage(file=images[68])
photo46 = photo46.zoom(newWH, newWH)

photo47 = PhotoImage(file=images[69])
photo47 = photo47.zoom(newWH, newWH)

photo48 = PhotoImage(file=images[71])
photo48 = photo48.zoom(newWH, newWH)

photo49 = PhotoImage(file=images[72])
photo49 = photo49.zoom(newWH, newWH)

photo50 = PhotoImage(file=images[73])
photo50 = photo50.zoom(newWH, newWH)

photo51 = PhotoImage(file=images[74])
photo51 = photo51.zoom(newWH, newWH)

photo52 = PhotoImage(file=images[75])
photo52 = photo52.zoom(newWH, newWH)

photo53 = PhotoImage(file=images[77])
photo53 = photo53.zoom(newWH, newWH)

photo54 = PhotoImage(file=images[80])
photo54 = photo54.zoom(newWH, newWH)

photo55 = PhotoImage(file=images[82])
photo55 = photo55.zoom(newWH, newWH)

photo56 = PhotoImage(file=images[83])
photo56 = photo56.zoom(newWH, newWH)

photo57 = PhotoImage(file=images[84])
photo57 = photo57.zoom(newWH, newWH)

photo58 = PhotoImage(file=images[85])
photo58 = photo58.zoom(newWH, newWH)

photo59 = PhotoImage(file=images[86])
photo59 = photo59.zoom(newWH, newWH)

photo60 = PhotoImage(file=images[87])
photo60 = photo60.zoom(newWH, newWH)

photo61 = PhotoImage(file=images[88])
photo61 = photo61.zoom(newWH, newWH)

photo62 = PhotoImage(file=images[89])
photo62 = photo62.zoom(newWH, newWH)

photo63 = PhotoImage(file=images[90])
photo63 = photo63.zoom(newWH, newWH)

photo64 = PhotoImage(file=images[91])
photo64 = photo64.zoom(newWH, newWH)

photo65 = PhotoImage(file=images[96])
photo65 = photo65.zoom(newWH, newWH)

photo66 = PhotoImage(file=images[97])
photo66 = photo66.zoom(newWH, newWH)

photo67 = PhotoImage(file=images[98])
photo67 = photo67.zoom(newWH, newWH)

photo68 = PhotoImage(file=images[102])
photo68 = photo68.zoom(newWH, newWH)

photo69 = PhotoImage(file=images[103])
photo69 = photo69.zoom(newWH, newWH)

photo70 = PhotoImage(file=images[104])
photo70 = photo70.zoom(newWH, newWH)

photo71 = PhotoImage(file=images[105])
photo71 = photo71.zoom(newWH, newWH)

photo72 = PhotoImage(file=images[106])
photo72 = photo72.zoom(newWH, newWH)

photo73 = PhotoImage(file=images[109])
photo73 = photo73.zoom(newWH, newWH)

photo74 = PhotoImage(file=images[112])
photo74 = photo74.zoom(newWH, newWH)

photo75 = PhotoImage(file=images[113])
photo75 = photo75.zoom(newWH, newWH)

photo76 = PhotoImage(file=images[114])
photo76 = photo76.zoom(newWH, newWH)

#Emote Images

photo77 = PhotoImage(file=images[1])
photo77 = photo77.zoom(newWH, newWH)

photo78 = PhotoImage(file=images[3])
photo78 = photo78.zoom(newWH, newWH)

photo79 = PhotoImage(file=images[6])
photo79 = photo79.zoom(newWH, newWH)

photo80 = PhotoImage(file=images[7])
photo80 = photo80.zoom(newWH, newWH)

photo81 = PhotoImage(file=images[8])
photo81 = photo81.zoom(newWH, newWH)

photo82 = PhotoImage(file=images[11])
photo82 = photo82.zoom(newWH, newWH)

photo83 = PhotoImage(file=images[12])
photo83 = photo83.zoom(newWH, newWH)

photo84 = PhotoImage(file=images[13])
photo84 = photo84.zoom(newWH, newWH)

photo85 = PhotoImage(file=images[14])
photo85 = photo85.zoom(newWH, newWH)

photo86 = PhotoImage(file=images[17])
photo86 = photo86.zoom(newWH, newWH)

photo87 = PhotoImage(file=images[30])
photo87 = photo87.zoom(newWH, newWH)

photo88 = PhotoImage(file=images[41])
photo88 = photo88.zoom(newWH, newWH)

photo89 = PhotoImage(file=images[44])
photo89 = photo89.zoom(newWH, newWH)

photo90 = PhotoImage(file=images[45])
photo90 = photo90.zoom(newWH, newWH)

photo91 = PhotoImage(file=images[48])
photo91 = photo91.zoom(newWH, newWH)

photo92 = PhotoImage(file=images[51])
photo92 = photo92.zoom(newWH, newWH)

photo93 = PhotoImage(file=images[61])
photo93 = photo93.zoom(newWH, newWH)

photo94 = PhotoImage(file=images[70])
photo94 = photo94.zoom(newWH, newWH)

photo95 = PhotoImage(file=images[78])
photo95 = photo95.zoom(newWH, newWH)

photo96 = PhotoImage(file=images[79])
photo96 = photo96.zoom(newWH, newWH)

photo97 = PhotoImage(file=images[92])
photo97 = photo97.zoom(newWH, newWH)

photo98 = PhotoImage(file=images[93])
photo98 = photo98.zoom(newWH, newWH)

photo99 = PhotoImage(file=images[94])
photo99 = photo99.zoom(newWH, newWH)

photo100 = PhotoImage(file=images[100])
photo100 = photo100.zoom(newWH, newWH)

photo101 = PhotoImage(file=images[101])
photo101 = photo101.zoom(newWH, newWH)

photo102 = PhotoImage(file=images[107])
photo102 = photo102.zoom(newWH, newWH)

photo103 = PhotoImage(file=images[115])
photo103 = photo103.zoom(newWH, newWH)

#extras

photo104 = PhotoImage(file=images[24])
photo104 = photo104.zoom(newWH, newWH)

photo105 = PhotoImage(file=images[37])
photo105 = photo105.zoom(newWH, newWH)

photo106 = PhotoImage(file=images[47])
photo106 = photo106.zoom(newWH, newWH)

photo107 = PhotoImage(file=images[76])
photo107 = photo107.zoom(newWH, newWH)

photo108 = PhotoImage(file=images[81])
photo108 = photo108.zoom(newWH, newWH)

photo109 = PhotoImage(file=images[95])
photo109 = photo109.zoom(newWH, newWH)

photo110 = PhotoImage(file=images[99])
photo110 = photo110.zoom(newWH, newWH)

photo111 = PhotoImage(file=images[108])
photo111 = photo111.zoom(newWH, newWH)

photo112 = PhotoImage(file=images[110])
photo112 = photo112.zoom(newWH, newWH)

photo113 = PhotoImage(file=images[111])
photo113 = photo113.zoom(newWH, newWH)

#couple more emotes

photo114 = PhotoImage(file=images[2])
photo114 = photo114.zoom(newWH, newWH)

photo115 = PhotoImage(file=images[32])
photo115 = photo115.zoom(newWH, newWH)

#Label Background

photo116 = PhotoImage(file=images[5])
photo116 = photo116.subsample(4)

#----------------------------------------------------------------------------------------------------
#Multiple Windows

class Page(Frame):
	def __init__(self, *args, **kwargs):
		Frame.__init__(self, *args, **kwargs)
		self['bg'] = 'black'
	def show(self):
		self.lift()

class Buttons(Page):
	def __init__(self, *args, **kwargs):
		Button.__init__(self,*args, **kwargs)
		self['bg'] = 'black'
		self['fg'] = 'white'
		self['width'] = imageWH
		self['height'] = imageWH

class Labels(Page):
	def __init__(self, *args, **kwargs):
		Button.__init__(self,*args, **kwargs)

class FlightPage(Page):
	def __init__(self, *args, **kwargs):
		Page.__init__(self, *args, **kwargs)

class FootPage(Page):
	def __init__(self, *args, **kwargs):
		Page.__init__(self, *args, **kwargs)

class EmotePage(Page):
	def __init__(self, *args, **kwargs):
		Page.__init__(self, *args, **kwargs)
	
class MainView(Frame):
	def __init__(self, *args, **kwargs):
		Frame.__init__(self, *args, **kwargs)
		p1 = FlightPage(self)
		p2 = FootPage(self)
		p3 = EmotePage(self)
		
		buttonframe = Frame(self)
		buttonframe2 = Frame(self, bg = 'black')
		container = Frame(self)
		buttonframe.pack(side="top", fill="x", expand=False)
		buttonframe2.pack(side="bottom", fill="x", expand=False)
		container.pack(side="top", fill="both", expand=True)
		
		p1.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
		p2.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
		p3.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
		
		b1 = Button(buttonframe, width= round(w/30), height = 5, text="Flight Controls", command = p1.lift, bg='black', fg='light blue', font = sizeFont)
		b2 = Button(buttonframe, width= round(w/30), height = 5, text="On-Foot Controls", command = p2.lift, bg='black', fg='light blue', font = sizeFont)
		b3 = Button(buttonframe, width= round(w/30), height = 5, text="Player Emotes", command  = p3.lift, bg='black', fg='light blue', font = sizeFont)
		
		b1.grid(row=0, column=0)
		b2.grid(row=0, column=1)
		b3.grid(row=0, column=2)

#----------------------------------------------------------------------------------------------------
#Buttons for Universal Controls

		labelBlank = Labels(buttonframe2, bg='black', image = photo116, width = round((w/2)-(imageWH*3.5)), height = 1, bd = 0)
		labelBlank.grid(row = 0, column = 0)
		
		buttonMB = Buttons(buttonframe2, image = photo44, command = lambda: toggle_mobiglass())
		buttonMB.grid(row = 0, column= 1)
		buttonSM = Buttons(buttonframe2, image = photo42, command = lambda: toggle_starmap())
		buttonSM.grid(row = 0, column= 2)
		buttonCV = Buttons(buttonframe2, image = photo7, command = lambda: toggle_camera_view())
		buttonCV.grid(row = 0, column= 3)
		buttonAccept = Buttons(buttonframe2, image = photo1, command = lambda: accept_invite())
		buttonAccept.grid(row = 0, column= 4)
		buttonReject = Buttons(buttonframe2, image = photo18, command = lambda: reject_invite())
		buttonReject.grid(row = 0, column= 5)
		buttonContacts = Buttons(buttonframe2, image = photo5, command = lambda: toggle_contacts())
		buttonContacts.grid(row = 0, column= 6)
		buttonGetUp = Buttons(buttonframe2, image = photo25, command = lambda: get_out())
		buttonGetUp.grid(row = 0, column= 7)
		
#----------------------------------------------------------------------------------------------------
#Buttons for Flight Controls

		labelBlank1 = Labels(p1, bg='black', image = photo116, width = round((w/2)-(imageWH*5.5)), height = 1, bd = 0) #width = 5      width = round(((w/2)-742.5))
		labelBlank1.grid(row = 0, column = 0)
		
		buttonIEP = Buttons(p1, image = photo24, text = "Eng Pwr+", compound = 'center', font = sizeFont2, command = lambda: incr_eng_pwr())
		buttonIEP.grid(row = 1, column = 9)
		buttonISP = Buttons(p1, image = photo63, text = "Shield Pwr+", compound = 'center', font = sizeFont2, command = lambda: incr_shld_pwr())
		buttonISP.grid(row = 1, column = 11)
		buttonIWP = Buttons(p1, image = photo75, text = "Weap Pwr+", compound = 'center', font = sizeFont2, command = lambda: incr_weap_pwr())
		buttonIWP.grid(row = 1, column = 10)
		buttonRP = Buttons(p1, image = photo50, text = "Reset Pwr", compound = 'center', font = sizeFont2, command = lambda: reset_pwr())
		buttonRP.grid(row = 2, column = 9)
		buttonDP = Buttons(p1, image = photo48, text = "Power-", compound = 'center', font = sizeFont2, command = lambda: decr_pwr())
		buttonDP.grid(row = 2, column = 8)
		buttonIP = Buttons(p1, image = photo51, text = "Power+", compound = 'center', font = sizeFont2, command = lambda: incr_pwr())
		buttonIP.grid(row = 1, column = 8)
		buttonRespawn = Buttons(p1, image = photo108, text = "Respawn", compound = 'center', font = sizeFont2, command = lambda: respawn_destruct())
		buttonRespawn.grid(row = 3, column = 3)
		buttonSQT = Buttons(p1, image = photo107, command = lambda: spool_qt())
		buttonSQT.grid(row = 1, column = 7)
		buttonQT = Buttons(p1, image = photo52, command = lambda: engage_qt())
		buttonQT.grid(row = 2, column = 7)
		buttonToggleVTOL = Buttons(p1, image = photo72, command = lambda: toggle_vtol())
		buttonToggleVTOL.grid(row = 3, column = 7)
		buttonAL = Buttons(p1, image = photo18, text = "AutoLand", compound = 'center', font = sizeFont2, command = lambda: auto_land())
		buttonAL.grid(row = 4, column = 8)
		buttonLG = Buttons(p1, image = photo39, command = lambda: re_lg())
		buttonLG.grid(row = 5, column = 8)
		button1stTarget = Buttons(p1, image = photo110, text = "Target#1", compound = 'center', font = sizeFont2, command = lambda: first_target())
		button1stTarget.grid(row = 0, column = 1)
		buttonP1stTarget = Buttons(p1, image = photo47, text = "Pin Target#1", compound = 'center', font = sizeFont2, command = lambda: pin_first_target())
		buttonP1stTarget.grid(row = 0, column = 2)
		button2ndTarget = Buttons(p1, image = photo110, text = "Target#2", compound = 'center', font = sizeFont2, command = lambda: second_target())
		button2ndTarget.grid(row = 0, column = 3)
		buttonP2ndTarget = Buttons(p1, image = photo47, text = "Pin Target#2", compound = 'center', font = sizeFont2, command = lambda: pin_second_target())
		buttonP2ndTarget.grid(row = 0, column = 4)
		button3rdTarget = Buttons(p1, image = photo110, text = "Target#3", compound = 'center', font = sizeFont2, command = lambda: third_target())
		button3rdTarget.grid(row = 0, column = 5)
		buttonP3rdTarget = Buttons(p1, image = photo47, text = "Pin Target#3", compound = 'center', font = sizeFont2, command = lambda: pin_third_target())
		buttonP3rdTarget.grid(row = 0, column = 6)
		buttonCycleAttackers = Buttons(p1, image = photo104, text = "Cycle Attackers", compound = 'center', font = sizeFont2, command = lambda: cycle_attackers())
		buttonCycleAttackers.grid(row = 1, column = 1)
		buttonResetAttackers = Buttons(p1, image = photo18, text = "Reset Attackers", compound = 'center', font = sizeFont2, command = lambda: reset_attackers())
		buttonResetAttackers.grid(row = 1, column = 2)
		buttonCycleHostiles = Buttons(p1, image = photo10, text = "Cycle Hostiles", compound = 'center', font = sizeFont2, command = lambda: cycle_hostiles())
		buttonCycleHostiles.grid(row = 1, column = 3)
		buttonResetHostiles = Buttons(p1, image = photo18, text = "Reset Hostiles", compound = 'center', font = sizeFont2, command = lambda: reset_hostiles())
		buttonResetHostiles.grid(row = 1, column = 4)
		buttonCycleFriendlies = Buttons(p1, image = photo12, text = "Cycle Friends", compound = 'center', font = sizeFont2, command = lambda: cycle_friendlies())
		buttonCycleFriendlies.grid(row = 1, column = 5)
		buttonResetFriendlies = Buttons(p1, image = photo18, text = "Reset Friends", compound = 'center', font = sizeFont2, command = lambda: reset_friendlies())
		buttonResetFriendlies.grid(row = 1, column = 6)
		buttonCycleAll = Buttons(p1, image = photo18, text = "Cycle All", compound = 'center', font = sizeFont2, command = lambda: cycle_all())
		buttonCycleAll.grid(row = 2, column = 1)
		buttonResetAll = Buttons(p1, image = photo18, text = "Reset All", compound = 'center', font = sizeFont2, command = lambda: reset_all())
		buttonResetAll.grid(row = 2, column = 2)
		buttonCycleSubs = Buttons(p1, image = photo18, text = "Cycle Subs", compound = 'center', font = sizeFont2, command = lambda: cycle_subs())
		buttonCycleSubs.grid(row = 2, column = 3)
		buttonResetSubs = Buttons(p1, image = photo53, text = "Reset Subs", compound = 'center', font = sizeFont2, command = lambda: reset_subs())
		buttonResetSubs.grid(row = 2, column = 4)
		buttonHailTarget = Buttons(p1, image = photo34, text = "Hail Target", compound = 'center', font = sizeFont2, command = lambda: hail_target())
		buttonHailTarget.grid(row = 3, column = 5)
		buttonClearPinTargets = Buttons(p1, image = photo18, text = "Clear Pins", compound = 'center', font = sizeFont2, command = lambda: clear_pin_targets())
		buttonClearPinTargets.grid(row = 2, column = 6)
		buttonToggleScanner = Buttons(p1, image = photo54, font = sizeFont2, command = lambda: toggle_scanner())
		buttonToggleScanner.grid(row = 4, column = 1)
		buttonFlightReady = Buttons(p1, image = photo66, font = sizeFont2, command = lambda: flight_ready())
		buttonFlightReady.grid(row = 3, column = 8)
		buttonResetSelect = Buttons(p1, image = photo18, text = "Reset Select", compound = 'center', font = sizeFont2, command = lambda: reset_selection())
		buttonResetSelect.grid(row = 2, column = 5)
		buttonRetLock = Buttons(p1, image = photo18, text = "Ret Lock", compound = 'center', font = sizeFont2, command = lambda: ret_lock())
		buttonRetLock.grid(row = 4, column = 4)
		buttonRetUnlock = Buttons(p1, image = photo18, text = "Ret Unlock", compound = 'center', font = sizeFont2, command = lambda: ret_unlock())
		buttonRetUnlock.grid(row = 5, column = 4)
		buttonSeatEject = Buttons(p1, image = photo18, text = "Eject", compound = 'center', font = sizeFont2, command = lambda: seat_eject())
		buttonSeatEject.grid(row = 5, column = 3)
		buttonTogglePwr = Buttons(p1, image = photo18, text = "Power On/Off", compound = 'center', font = sizeFont2, command = lambda: toggle_pwr())
		buttonTogglePwr.grid(row = 0, column = 8)
		buttonEmSeatExit = Buttons(p1, image = photo105, font = sizeFont2, command = lambda: em_seat_exit())
		buttonEmSeatExit.grid(row = 4, column = 3)
		buttonToggleEngines = Buttons(p1, image = photo23, text = "Engines On/Off", compound = 'center', font = sizeFont2, command = lambda: toggle_engs())
		buttonToggleEngines.grid(row = 0, column = 9)
		buttonToggleShields = Buttons(p1, image = photo63, text = "Shields On/Off", compound = 'center', font = sizeFont2, command = lambda: toggle_shlds())
		buttonToggleShields.grid(row = 0, column = 11)
		buttonToggleWeapons = Buttons(p1, image = photo75, text = "Weapons On/Off", compound = 'center', font = sizeFont2, command = lambda: toggle_weaps())
		buttonToggleWeapons.grid(row = 0, column = 10)
		buttonToggleGimbal = Buttons(p1, image = photo29, text = "Gimbals", compound = 'center', font = sizeFont2, command = lambda: toggle_gimbal())
		buttonToggleGimbal.grid(row = 5, column = 2)
		buttonLaunchCM = Buttons(p1, image = photo8, text = "Launch CM", compound = 'center', font = sizeFont2, command = lambda: launch_cm())
		buttonLaunchCM.grid(row = 3, column = 1)
		buttonCycleCM = Buttons(p1, image = photo109, text = "Cycle CM", compound = 'center', font = sizeFont2, command = lambda: cycle_cm())
		buttonCycleCM.grid(row = 3, column = 2)
		buttonJettisonCargo = Buttons(p1, image = photo18, text = "Jettison Cargo", compound = 'center', font = sizeFont2, command = lambda: jettison_Cargo())
		buttonJettisonCargo.grid(row = 4, column = 2)
		buttonShipLights = Buttons(p1, image = photo64, command = lambda: toggle_lights())
		buttonShipLights.grid(row = 4, column = 7)
		buttonToggleCruise = Buttons(p1, image = photo6, font = sizeFont2, command = lambda: toggle_cruise_control())
		buttonToggleCruise.grid(row =5, column = 7)
		buttonToggleDecoupled = Buttons(p1, image = photo18, text = "Decoupled", compound = 'center', font = sizeFont2, command = lambda: toggle_decoupled())
		buttonToggleDecoupled.grid(row = 4, column = 5)
		buttonToggleMining = Buttons(p1, image = photo65, text = "Mining", compound = 'center', font = sizeFont2, command = lambda: toggle_mining())
		buttonToggleMining.grid(row = 5, column = 1)
		buttonToggleAITurrets = Buttons(p1, image = photo18, text = "AI turrets", compound = 'center', font = sizeFont2, command = lambda: toggle_ai_turrets())
		buttonToggleAITurrets.grid(row =5, column = 5)
		buttonShldRaiseTop = Buttons(p1, image = photo61, text = "Shield Top+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_top())
		buttonShldRaiseTop.grid(row = 3, column = 11)
		buttonShldRaiseFront = Buttons(p1, image = photo57, text = "Shield Front+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_front())
		buttonShldRaiseFront.grid(row = 3, column = 10)
		buttonShldRaiseBottom = Buttons(p1, image = photo56, text = "Shield Bottom+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_bottom())
		buttonShldRaiseBottom.grid(row = 5, column = 11)
		buttonShldRaiseLeft = Buttons(p1, image = photo58, text = "Shield Left+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_left())
		buttonShldRaiseLeft.grid(row = 4, column = 9)
		buttonShldRaiseRight = Buttons(p1, image = photo60, text = "Shield Right+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_right())
		buttonShldRaiseRight.grid(row = 4, column = 11)
		buttonShldRaiseBack = Buttons(p1, image = photo55, text = "Shield Back+", compound = 'center', font = sizeFont2, command = lambda: shld_raise_back())
		buttonShldRaiseBack.grid(row = 5, column = 10)
		buttonShldReset = Buttons(p1, image = photo59, text = "Shield Reset", compound = 'center', font = sizeFont2, command = lambda: shld_reset())
		buttonShldReset.grid(row = 4, column = 10)
		buttonToggleDoorLocks = Buttons(p1, image = photo19, command = lambda: toggle_door_lock())
		buttonToggleDoorLocks.grid(row = 4, column = 6)
		buttonOpenCloseDoors = Buttons(p1, image = photo20, command = lambda: open_close_doors())
		buttonOpenCloseDoors.grid(row = 5, column = 6)

#----------------------------------------------------------------------------------------------------
#Buttons for On-Foot Controls

		labelBlank2 = Labels(p2, bg='black', image = photo116, width = round((w/2)-(imageWH*3.5)), height = 1, bd = 0)
		labelBlank2.grid(row = 0, rowspan = 5, column = 0)
		
		buttonSideArm = Buttons(p2, image = photo112, command = lambda: equip_sidearm())
		buttonSideArm.grid(row = 0, column = 1)
		buttonEquip1stPrimary = Buttons(p2, image = photo111, command = lambda: equip_first_primary())
		buttonEquip1stPrimary.grid(row = 0, column = 2)
		buttonEquip2ndPrimary = Buttons(p2, image = photo113, command = lambda: equip_second_primary())
		buttonEquip2ndPrimary.grid(row = 0, column = 3)
		buttonEquipGadget = Buttons(p2, image = photo106, command = lambda: equip_gadget())
		buttonEquipGadget.grid(row = 0, column = 4)
		buttonCycleMelee = Buttons(p2, image = photo38, command = lambda: cycle_melee())
		buttonCycleMelee.grid(row = 0, column = 5)
		buttonMissionItem = Buttons(p2, image = photo18, text = "Mission Item", compound = 'center', font = sizeFont2, command = lambda: mission_item())
		buttonMissionItem.grid(row = 0, column = 6)
		buttonUnarmed = Buttons(p2, image = photo26, command = lambda: unarmed())
		buttonUnarmed.grid(row = 0, column = 7)
		buttonHolster = Buttons(p2, image = photo18, text = "Holster", compound = 'center', font = sizeFont, command = lambda: holster())
		buttonHolster.grid(row = 1, column = 3)
		buttonFireMode = Buttons(p2, image = photo18, text = "Fire Mode", compound = 'center', font = sizeFont, command = lambda: fire_modes())
		buttonFireMode.grid(row = 1, column = 4)
		buttonWeapCustom = Buttons(p2, image = photo18, text = "Weap Customize", compound = 'center', font = sizeFont2, command = lambda: weap_custom())
		buttonWeapCustom.grid(row = 1, column = 5)
		buttonOxyRefill = Buttons(p2, image = photo46, command = lambda: oxy_refill())
		buttonOxyRefill.grid(row = 2, column = 4)
		buttonSelfHeal = Buttons(p2, image = photo43, command = lambda: self_heal())
		buttonSelfHeal.grid(row = 2, column = 3)
		buttonInventory = Buttons(p2, image = photo36, command = lambda: inventory())
		buttonInventory.grid(row = 4, column = 2)
		buttonFlashlight = Buttons(p2, image = photo27, command = lambda: flashlight())
		buttonFlashlight.grid(row = 2, column = 5)
		
		#----------------------------------------------------------------------------------------------------
		#Buttons for Camera Controls on the On-Foot Page
		
		buttonCamDOFDecr = Buttons(p2, image = photo18, text = "DOF-", compound = 'center', font = sizeFont, command = lambda: decr_dof())
		buttonCamDOFDecr.grid(row = 4, column = 1)
		buttonCamDOFIncr = Buttons(p2, image = photo18, text = "DOF+", compound = 'center', font = sizeFont, command = lambda: incr_dof())
		buttonCamDOFIncr.grid(row = 4, column = 3)
		buttonCamZOffsetPos = Buttons(p2, image = photo18, text = "Z+", compound = 'center', font = sizeFont, command = lambda: z_offset_pos())
		buttonCamZOffsetPos.grid(row = 3, column = 2)
		buttonCamZOffsetNeg = Buttons(p2, image = photo18, text = "Z-", compound = 'center', font = sizeFont, command = lambda: z_offset_neg())
		buttonCamZOffsetNeg.grid(row = 5, column = 2)
		buttonCamYOffsetPos = Buttons(p2, image = photo18, text = "Y+", compound = 'center', font = sizeFont, command = lambda: y_offset_pos())
		buttonCamYOffsetPos.grid(row = 3, column = 6)
		buttonCamYOffsetNeg = Buttons(p2, image = photo18, text = "Y-", compound = 'center', font = sizeFont, command = lambda: y_offset_neg())
		buttonCamYOffsetNeg.grid(row = 5, column = 6)
		buttonCamXOffsetNeg = Buttons(p2, image = photo18, text = "X-", compound = 'center', font = sizeFont, command = lambda: x_offset_neg())
		buttonCamXOffsetNeg.grid(row = 4, column = 5)
		buttonCamViewReset = Buttons(p2, image = photo18, text = "Reset", compound = 'center', font = sizeFont, command = lambda: reset_view())
		buttonCamViewReset.grid(row = 4, column = 6)
		buttonCamXOffsetPos = Buttons(p2, image = photo18, text = "X+", compound = 'center', font = sizeFont, command = lambda: x_offset_pos())
		buttonCamXOffsetPos.grid(row = 4, column = 7)

#----------------------------------------------------------------------------------------------------
#Buttons for Player Emotes

		labelBlank3 = Labels(p3, bg='black', image = photo116, width = round((w/2)-(imageWH*5)), height = 1, bd = 0)
		labelBlank3.grid(row = 0, column = 0)
		
		buttonAgree = Buttons(p3, image = photo18, text = "Agree", compound = 'center', font = sizeFont, command = lambda: emote_agree())
		buttonAgree.grid(row=0,column = 1)
		buttonAngry = Buttons(p3, image = photo77, command = lambda: emote_angry())
		buttonAngry.grid(row=0,column = 2)
		buttonAtEase = Buttons(p3, image = photo114, command = lambda: emote_at_ease())
		buttonAtEase.grid(row=0,column = 3)
		buttonAttention = Buttons(p3, image = photo78, command = lambda: emote_attention())
		buttonAttention.grid(row=0,column = 4)
		buttonBlah = Buttons(p3, image = photo18, text = "Blah", compound = 'center', font = sizeFont, command = lambda: emote_blah())
		buttonBlah.grid(row=0,column = 5)
		buttonBored = Buttons(p3, image = photo79, command = lambda: emote_bored())
		buttonBored.grid(row=0,column = 6)
		buttonBow = Buttons(p3, image = photo80, command = lambda: emote_bow())
		buttonBow.grid(row=0,column = 7)
		buttonBurp = Buttons(p3, image = photo81, command = lambda: emote_burp())
		buttonBurp.grid(row=0,column = 8)
		buttonCheer = Buttons(p3, image = photo82, command = lambda: emote_cheer())
		buttonCheer.grid(row=0,column = 9)
		buttonChicken = Buttons(p3, image = photo83, command = lambda: emote_chicken())
		buttonChicken.grid(row=0,column = 10)
		buttonClap = Buttons(p3, image = photo84, command = lambda: emote_clap())
		buttonClap.grid(row=1,column = 1)
		buttonCome = Buttons(p3, image = photo85, command = lambda: emote_come())
		buttonCome.grid(row=1,column = 2)
		buttonCry = Buttons(p3, image = photo86, command = lambda: emote_cry())
		buttonCry.grid(row=1,column = 3)
		buttonDance = Buttons(p3, image = photo87, command = lambda: emote_dance())
		buttonDance.grid(row=1,column = 4)
		buttonAtEase = Buttons(p3, image = photo115, command = lambda: emote_disagree())
		buttonAtEase.grid(row=1,column = 5)
		buttonFailure = Buttons(p3, image = photo88, command = lambda: emote_failure())
		buttonFailure.grid(row=1,column = 6)
		buttonFlex = Buttons(p3, image = photo89, command = lambda: emote_flex())
		buttonFlex.grid(row=1,column = 7)
		buttonFlirt = Buttons(p3, image = photo90, command = lambda: emote_flirt())
		buttonFlirt.grid(row=1,column = 8)
		buttonGasp = Buttons(p3, image = photo91, command = lambda: emote_gasp())
		buttonGasp.grid(row=1,column = 9)
		buttonGloat = Buttons(p3, image = photo18, text = "Gloat", compound = 'center', font = sizeFont, command = lambda: emote_gloat())
		buttonGloat.grid(row=1,column = 10)
		buttonGreet = Buttons(p3, image = photo92, command = lambda: emote_greet())
		buttonGreet.grid(row=2,column = 1)
		buttonLaugh = Buttons(p3, image = photo93, command = lambda: emote_laugh())
		buttonLaugh.grid(row=2,column = 2)
		buttonPoint = Buttons(p3, image = photo94, command = lambda: emote_point())
		buttonPoint.grid(row=2,column = 3)
		buttonRude = Buttons(p3, image = photo95, command = lambda: emote_rude())
		buttonRude.grid(row=2,column = 4)
		buttonSalute = Buttons(p3, image = photo96, command = lambda: emote_salute())
		buttonSalute.grid(row=2,column = 5)
		buttonSit = Buttons(p3, image = photo97, command = lambda: emote_sit())
		buttonSit.grid(row=2,column = 6)
		buttonSleep = Buttons(p3, image = photo98, command = lambda: emote_sleep())
		buttonSleep.grid(row=2,column = 7)
		buttonSmell = Buttons(p3, image = photo99, command = lambda: emote_smell())
		buttonSmell.grid(row=2,column = 8)
		buttonStand = Buttons(p3, image = photo18, text = "Stand", compound = 'center', font = sizeFont, command = lambda: emote_stand())
		buttonStand.grid(row=2,column = 9)
		buttonTaunt = Buttons(p3, image = photo100, command = lambda: emote_taunt())
		buttonTaunt.grid(row=2,column = 10)
		buttonThreaten = Buttons(p3, image = photo101, command = lambda: emote_threaten())
		buttonThreaten.grid(row=3,column = 1)
		buttonWait = Buttons(p3, image = photo102, command = lambda: emote_wait())
		buttonWait.grid(row=3,column = 2)
		buttonWave = Buttons(p3, image = photo18, text = "Wave", compound = 'center', font = sizeFont, command = lambda: emote_wave())
		buttonWave.grid(row=3,column =3)
		buttonWhistle = Buttons(p3, image = photo103, command = lambda: emote_whistle())
		buttonWhistle.grid(row=3,column = 4)
		
		p1.show()

#----------------------------------------------------------------------------------------------------
#execution of GUI

if __name__ == "__main__":
	main = MainView(root)
	main.pack(side="top", fill="both", expand=True)
	
#----------------------------------------------------------------------------------------------------
#Enabling Fullscreen Mode

	root.overrideredirect(1)
	root.geometry("%dx%d+0+0" % (w,h))
	root.focus_set() #focus
	root.bind("<Escape>", lambda e: e.widget.quit()) #Press escape to close

#----------------------------------------------------------------------------------------------------
#Server Address	Prompt

	url = askstring(title = 'IP Address',  prompt = "Enter your server\'s local IP Address(ex. 192.168.1.1): ") #, initialvalue = "0.0.0.0")
	url = 'http://' + url + ':5000/'

#----------------------------------------------------------------------------------------------------
#Executing program
	main.mainloop()