# Star Citizen Panel

Worked on this project since there were no windows based panels, I didn't want to buy an android tablet, an ipad, or an elgato streamdeck. This led me to just making the project overall in python. Used pyDirectInput, flask, openCV, Requests, tkinter.

Would upload the executible for the Star Citizen Panel itself, but gitlab limits file sizes to 10mb, and my program is 48mb since everything except the server is built into the exe.
So here is my onedrive link for it.
https://1drv.ms/u/s!Ahq3T6gWLHwQi4tGI3TlhL8arctxNw?e=nIGTpG

To Properly run:
Run Star Citizen Panel Server.exe on the computer you will be playing Star Citizen, and run Star Citizen Panel.exe on your windows-based tablet or laptop (ideally has a touch screen). It will ask for the ip address where the Server is running on, if you don't know, then right-click on the start button, click on windows command prompt or windows powershell, type in "ipconfig" without the quotes, and search for an adapter that has "IPvv4 Address" there may be more than one so make sure it looks something like "192.168.1.110" or "192.168.0.115" usually along those lines. If you see an adapter listed with "VMWare Network Adapter" that is not the right adapter. Once you have identified your ip address, type it into the panel's dialogue prompt, once you press enter, it should enter fullscreen. I have noticed that moving the dialogue prompt around can force the program to not fullscreen properly and if it doesn't then just re-run the program. To exit the Panel, simply press Escape.



This is still early-ish in its development, 34 buttons dont have the appropriate image because they need to be acutally created from scratch.


Includes ability to set ip address of the computer that is playing star citizen so it correctly connects to the Server.



Troubleshooting:
Q:"Panel isnt connected to the server even though my ip address is correct"
A: ensure you allowed the server to run on your computer when first launching, if you did, then the panel itself might be blocked. easiest way is to run the server where you have the panel to get the firewall prompt, then you can exit the server and relaunch the panel program and it should connect to your desktop now.