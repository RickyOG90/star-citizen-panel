import flask
from flask import (Flask, render_template)
import time
#import pydirectinput as pydi
import newDirectInput as pydi2


app = Flask(__name__)
@app.route("/keypress/<string:key>")
def test(key):
	#time.sleep(0.5)
	pydi2.press(key)
	return "done"

@app.route("/hotkey/<string:key2>")
def keycombo(key2):
	#time.sleep(0.5)
	if key2 == 'alt1':
		pydi2.hotkey('altleft', '1')
		return "key " + key2 + " pressed"
	if key2 == 'alt2':
		pydi2.hotkey('altleft', '2')
		return "key " + key2 + " pressed"
	if key2 == 'alt3':
		pydi2.hotkey('altleft', '3')
		return "key " + key2 + " pressed"
	if key2 == 'alt4':
		pydi2.hotkey('altleft', '4')
		return "key " + key2 + " pressed"
	if key2 == 'alt5':
		pydi2.hotkey('altleft', '5')
		return "key " + key2 + " pressed"
	if key2 == 'alt6':
		pydi2.hotkey('altleft', '6')
		return "key " + key2 + " pressed"
	if key2 == 'alt7':
		pydi2.hotkey('altleft', '7')
		return "key " + key2 + " pressed"
	if key2 == 'alt8':
		pydi2.hotkey('altleft', '8')
		return "key " + key2 + " pressed"
	if key2 == 'altr':
		pydi2.hotkey('altleft', 'r')
		return "key " + key2 + " pressed"
	if key2 == 'altt':
		pydi2.hotkey('altleft', 't')
		return "key " + key2 + " pressed"
	if key2 == 'shiftu':
		pydi2.hotkey('shiftleft', 'u')
		return "key " + key2 + " pressed"
	if key2 == 'f4home':
		pydi2.hotkey('f4', 'home')
		return "key " + key2 + " pressed"
	if key2 == 'f4pageup':
		pydi2.hotkey('f4', 'pageup')
		return "key " + key2 + " pressed"
	if key2 == 'f4pagedown':
		pydi2.hotkey('f4', 'pagedown')
		return "key " + key2 + " pressed"
	if key2 == 'f4up':
		pydi2.hotkey('f4', 'up')
		return "key " + key2 + " pressed"
	if key2 == 'f4down':
		pydi2.hotkey('f4', 'down')
		return "key " + key2 + " pressed"
	if key2 == 'f4right':
		pydi2.hotkey('f4', 'right')
		return "key " + key2 + " pressed"
	if key2 == 'f4left':
		pydi2.hotkey('f4', 'left')
		return "key " + key2 + " pressed"
	if key2 == 'f4num*':
		pydi2.hotkey('f4', 'multiply')
		return "key " + key2 + " pressed"
	if key2 == 'f4num-':
		pydi2.hotkey('f4', 'subtract')
		return "key " + key2 + " pressed"
	if key2 == 'rr':
		pydi2.keyDown('r')
		time.sleep(1)
		pydi2.keyUp('r')
		return "key " + key2 + " pressed"
	if key2 == 'backspace':
		pydi2.keyDown('backspace')
		time.sleep(1)
		pydi2.keyUp('backspace')
		return "key " + key2 + " pressed"
	if key2 == 'j':
		pydi2.keyDown('j')
		time.sleep(1)
		pydi2.keyUp('j')
		return "key " + key2 + " pressed"
	if key2 == 'l':
		pydi2.keyDown('l')
		time.sleep(1)
		pydi2.keyUp('l')
		return "key " + key2 + " pressed"
	if key2 == 'b':
		pydi2.keyDown('b')
		time.sleep(1)
		pydi2.keyUp('b')
		return "key " + key2 + " pressed"
	if key2 == 'n':
		pydi2.keyDown('n')
		time.sleep(1)
		pydi2.keyUp('n')
		return "key " + key2 + " pressed"
	if key2 == 'shift6':
		pydi2.hotkey('shiftleft', '6')
		return "key " + key2 + " pressed"
	if key2 == 'shift7':
		pydi2.hotkey('shiftleft', '7')
		return "key " + key2 + " pressed"
	if key2 == 'shift8':
		pydi2.hotkey('shiftleft', '8')
		return "key " + key2 + " pressed"
	if key2 == 'shift9':
		pydi2.hotkey('shiftleft', '9')
		return "key " + key2 + " pressed"
	if key2 == 'shift0':
		pydi2.hotkey('shiftleft', '0')
		return "key " + key2 + " pressed"
	if key2 == 'shift-':
		pydi2.hotkey('shiftleft', '-')
		return "key " + key2 + " pressed"
	if key2 == 'shift=':
		pydi2.hotkey('shiftleft', '=')
		return "key " + key2 + " pressed"
	if key2 == 'shift\'':
		pydi2.hotkey('shiftleft', '\'')
		return "key " + key2 + " pressed"
	if key2 == 'alt6':
		pydi2.hotkey('altleft', '6')
		return "key " + key2 + " pressed"
	if key2 == 'alt7':
		pydi2.hotkey('altleft', '7')
		return "key " + key2 + " pressed"
	if key2 == 'alt8':
		pydi2.hotkey('altleft', '8')
		return "key " + key2 + " pressed"
	if key2 == 'alt9':
		pydi2.hotkey('altleft', '9')
		return "key " + key2 + " pressed"
	if key2 == 'alt0':
		pydi2.hotkey('altleft', '0')
		return "key " + key2 + " pressed"
	if key2 == 'alt-':
		pydi2.hotkey('altleft', '-')
		return "key " + key2 + " pressed"
	if key2 == 'alt=':
		pydi2.hotkey('altleft', '=')
		return "key " + key2 + " pressed"
	if key2 == 'alt\'':
		pydi2.hotkey('altleft', '\'')
		return "key " + key2 + " pressed"
	if key2 == 'altshift6':
		pydi2.hotkey('altleft', 'shiftleft', '6')
		return "key " + key2 + " pressed"
	if key2 == 'altshift7':
		pydi2.hotkey('altleft', 'shiftleft', '7')
		return "key " + key2 + " pressed"
	if key2 == 'altshift8':
		pydi2.hotkey('altleft', 'shiftleft', '8')
		return "key " + key2 + " pressed"
	if key2 == 'altshift9':
		pydi2.hotkey('altleft', 'shiftleft', '9')
		return "key " + key2 + " pressed"
	if key2 == 'stand':
		pydi2.press('enter')
		pydi2.write('/stand')
		pydi2.press('enter')
		return "stand " " typed"
	if key2 == 'altshift0':
		pydi2.hotkey('altleft', 'shiftleft', '0')
		return "key " + key2 + " pressed"
	if key2 == 'altshift-':
		pydi2.hotkey('altleft', 'shiftleft', '-')
		return "key " + key2 + " pressed"
	if key2 == 'altshift=':
		pydi2.hotkey('altleft', 'shiftleft', '=')
		return "key " + key2 + " pressed"
	if key2 == 'altshift\'':
		pydi2.hotkey('altleft', 'shiftleft', '\'')
		return "key " + key2 + " pressed"
	if key2 == 'altshift\\':
		pydi2.hotkey('altleft', 'shiftleft', '\\')
		return "key " + key2 + " pressed"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)